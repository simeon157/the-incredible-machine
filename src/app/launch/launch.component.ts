import { Component, OnInit } from '@angular/core';
import { LaunchDataService } from '../launch-data.service';
import { DomSanitizer } from '@angular/platform-browser';
import { ActivatedRoute } from '@angular/router';
import { identifierName } from '@angular/compiler';

@Component({
  selector: 'app-launch',
  templateUrl: './launch.component.html',
  styleUrls: ['./launch.component.scss'],
  providers: [LaunchDataService]
})
export class LaunchComponent implements OnInit {
  launch: any;
  loaded: boolean = false;
  launchSuccess: boolean;
  flightNumber: number;
  missionPatch: string;
  launchDetails: string;
  rocketName: string;
  rocketType: string;
  launchSite: string;
  launchDate: number;
  constructor(LaunchDataService: LaunchDataService, public sanitizer: DomSanitizer, private route:ActivatedRoute) {
    this.sanitizer = sanitizer;
    let currentRoute = this.route.snapshot.params;
    let id = '';
    if(currentRoute.id) {
      id = currentRoute.id;
    }
    LaunchDataService.getLaunches('', id).subscribe(
      launch => {
        // Api returns an array of launches, but we need get only one element.
        launch = launch[0];
        console.log("LATEST LAUNCH RECEIVED: ", launch);
        this.launch = launch[0];
        this.launchSuccess = launch.launch_success;
        this.loaded = true;
        this.flightNumber = launch.flight_number;
        this.missionPatch = launch.links.mission_patch_small;
        this.launchDetails = launch.details;
        this.rocketName = launch.rocket.rocket_name;
        this.rocketType = launch.rocket.rocket_type;
        this.launchSite = launch.launch_site.site_name_long;

         // Angular doesn't like seconds timestamps.
         this.launchDate = launch.launch_date_unix * 1000;
      },
      x => {
        console.log("ERROR: ",x);
      },
      () => {
        console.log("Completed");
      }
    );
  }

  ngOnInit() {
    
  }

}
