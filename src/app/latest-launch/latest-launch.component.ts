import { Component, OnInit, ViewChild, ViewContainerRef } from "@angular/core";
import { LaunchDataService } from "../launch-data.service";
import { DomSanitizer } from "@angular/platform-browser";

@Component({
  selector: "app-latest-launch",
  templateUrl: "./latest-launch.component.html",
  styleUrls: ["../latest-launch/latest-launch.component.scss"],
  providers: [LaunchDataService]
})
export class LatestLaunchComponent implements OnInit {
  
  // Launch variables from the SpaceX API.
  launch: any;
  loaded: boolean = false;
  launchSuccess: boolean;
  flightNumber: number;
  missionPatch: string;
  launchDetails: string;
  rocketName: string;
  rocketType: string;
  launchSite: string;
  launchDate: number;
  constructor(
    LaunchDataService: LaunchDataService,
    public sanitizer: DomSanitizer
  ) {
    this.sanitizer = sanitizer;
    LaunchDataService.getLaunches("latest").subscribe(
      launch => {
        this.launch = launch;
        this.launchSuccess = launch.launch_success;
        this.flightNumber = launch.flight_number;
        this.missionPatch = launch.links.mission_patch_small;
        this.launchDetails = launch.details;
        this.rocketName = launch.rocket.rocket_name;
        this.rocketType = launch.rocket.rocket_type;
        this.launchSite = launch.launch_site.site_name_long;
        // Angular doesn't like seconds timestamps.
        this.launchDate = launch.launch_date_unix * 1000;
      },
      x => {
        console.log("ERROR: ", x);
      },
      () => {
        this.loaded = true;
      }
    );
  }

  ngOnInit() {}
}
