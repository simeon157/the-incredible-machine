import { Component, OnInit } from '@angular/core';
import { LaunchDataService } from '../launch-data.service';


@Component({
  selector: 'app-past-launches',
  templateUrl: './past-launches.component.html',
  styleUrls: ['./past-launches.component.scss'],
  providers: [LaunchDataService]
})
export class PastLaunchesComponent implements OnInit {
  
  launches: Array<object>;
  error: string;
  loaded:boolean = false;
  launchDate:string;
  constructor(LaunchDataService: LaunchDataService) {
    console.log(this.loaded);
    LaunchDataService.getLaunches('past').subscribe(
      launches => {
        this.launches = launches;
        this.loaded = true;
      },
      error => {
        this.error = error;
      }
    );
  }

  ngOnInit() {
  }
}
