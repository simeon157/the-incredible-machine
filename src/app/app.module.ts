import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';
import { LatestLaunchComponent } from './latest-launch/latest-launch.component';
import { UpcomingLaunchesComponent } from './upcoming-launches/upcoming-launches.component';
import { PastLaunchesComponent } from './past-launches/past-launches.component';
import { LaunchComponent } from './launch/launch.component';
import { NavbarComponent } from './navbar/navbar.component';
import { FrontpageHeaderComponent } from './frontpage-header/frontpage-header.component';
import { LaunchBoxComponent } from './launch-box/launch-box.component';
import { RocketLoaderComponent } from './rocket-loader/rocket-loader.component';
import { HomeButtonComponent } from './home-button/home-button.component';

const appRoutes: Routes = [
  { path: '', component: FrontpageHeaderComponent },
  { path: 'latest', component: LatestLaunchComponent },
  { path: 'past', component: PastLaunchesComponent },
  { path: 'upcoming', component: UpcomingLaunchesComponent },
  { path: 'launch/:id', component: LaunchComponent },
];

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    FrontpageHeaderComponent,
    LatestLaunchComponent,
    LaunchComponent,
    PastLaunchesComponent,
    UpcomingLaunchesComponent,
    LaunchBoxComponent,
    RocketLoaderComponent,
    HomeButtonComponent,
  ],
  imports: [
    BrowserModule,
    HttpModule,
    RouterModule.forRoot(appRoutes),
  ],
  providers: [HttpClientModule],
  bootstrap: [AppComponent]
})
export class AppModule { }
