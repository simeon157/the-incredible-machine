import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FrontpageHeaderComponent } from './frontpage-header.component';

describe('FrontpageHeaderComponent', () => {
  let component: FrontpageHeaderComponent;
  let fixture: ComponentFixture<FrontpageHeaderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FrontpageHeaderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FrontpageHeaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
