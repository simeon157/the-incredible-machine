import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LaunchBoxComponent } from './launch-box.component';

describe('LaunchBoxComponent', () => {
  let component: LaunchBoxComponent;
  let fixture: ComponentFixture<LaunchBoxComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LaunchBoxComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LaunchBoxComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
