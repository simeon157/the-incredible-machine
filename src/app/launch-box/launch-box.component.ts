import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-launch-box',
  templateUrl: './launch-box.component.html',
  styleUrls: ['./launch-box.component.scss']
})
export class LaunchBoxComponent implements OnInit {

  @Input() launch: any;
  upcomming = false;
  launchDate:number;
  constructor() {
  }

  isUpcomming() {
    if(this.launch.upcomming === true) {
      return true;
    }
    else {
      return false;
    }
  }

  ngOnInit() {}
}
