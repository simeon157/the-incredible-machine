import { Component, OnInit } from '@angular/core';
import { LaunchDataService } from '../launch-data.service';

@Component({
  selector: 'app-rocket-loader',
  templateUrl: './rocket-loader.component.html',
  styleUrls: ['./rocket-loader.component.scss']
})
export class RocketLoaderComponent implements OnInit {
  constructor(LaunchDataService:LaunchDataService) {
  }

  ngOnInit() {
  }

}
