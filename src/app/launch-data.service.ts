import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable'

import 'rxjs/add/operator/map';

@Injectable()
export class LaunchDataService {

  constructor(private http: Http) {}

  private apiUrl = 'https://api.spacexdata.com/v2/';
  getLaunches(launchType = 'latest', flight_number = '') {
    let launchApiUrl = this.apiUrl;
  
    switch (launchType) {
      case 'upcoming':
        launchApiUrl += 'launches/upcoming';
        break;
      case 'past':
        launchApiUrl += 'launches';
        break;
      case 'latest':
        launchApiUrl += 'launches/latest/';
        break;
      default:
        launchApiUrl += 'launches' + '?flight_number=' + flight_number;
        break;
    }
    return this.http.get(launchApiUrl).map(this.extractData);
  }

  private extractData(res: Response) {
    let body = res.json();

    return body || {};
  }

  private handleError (error: any) {
    // In a real world app, we might use a remote logging infrastructure
    // We'd also dig deeper into the error to get a better message
    let errMsg = (error.message) ? error.message :
      error.status ? `${error.status} - ${error.statusText}` : 'Server error';
    console.error(errMsg); // log to console instead
    return Observable.throw(errMsg);
  }
}
