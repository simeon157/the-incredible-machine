import { Component, OnInit } from '@angular/core';
import { LaunchDataService } from '../launch-data.service';

@Component({
  selector: 'app-upcoming-launches',
  templateUrl: './upcoming-launches.component.html',
  styleUrls: ['../past-launches/past-launches.component.scss'],
  providers: [LaunchDataService]
})
export class UpcomingLaunchesComponent implements OnInit {

  launches: any;
  error: string;
  loaded:boolean = false;
  constructor(LaunchDataService: LaunchDataService) {
    LaunchDataService.getLaunches('upcoming').subscribe(
      launches => {
        this.launches = launches;
        this.loaded = true;

        this.launches.forEach(element => {
          element.upcomming = true;
        });
        console.log(this.launches);
      },
    );
  }

  ngOnInit() {
  }

}
