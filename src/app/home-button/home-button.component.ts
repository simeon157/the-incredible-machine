import { Component, OnInit,  } from '@angular/core';
import { Location } from '@angular/common';
import { Router } from '@angular/router';

@Component({
  selector: 'app-home-button',
  templateUrl: './home-button.component.html',
  styleUrls: ['./home-button.component.scss']
})
export class HomeButtonComponent implements OnInit {

  home: boolean;

  constructor(Router:Router, location: Location) {
    Router.events.subscribe((val) => {
      if(location.path() !== '') {
        console.log('hai');
        this.home = false;
      } 
      else {
        this.home = true;
      }
    });
    console.log(this.home);
   }

  ngOnInit() {
  }

}
